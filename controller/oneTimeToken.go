package controller

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/gorilla_morimoto/board/models"

	_ "github.com/lib/pq" // 直接使用しないためブランクインポートにしています
)

func saveOneTimeToken(oneTimeToken string) error {
	db, err := models.WhichDB()
	defer db.Close()

	if err != nil {
		log.Println("ERROR: failed to connect DB")
		return err
	}
	stmt, _ := db.Prepare(`insert into tokens(token, created_at) values($1, $2)`)

	_, err = stmt.Exec(oneTimeToken, time.Now())
	// errの場合はリトライする
	// それでもだめならエラーを返す
	if err != nil {
		_, err := stmt.Exec(oneTimeToken, time.Now())
		if err != nil {
			return err
		}
	}
	return nil
}

func isValidOneTimeToken(oneTimeToken string) (bool, error) {
	db, err := models.WhichDB()
	defer db.Close()

	if err != nil {
		log.Println("ERROR: failed to connect DB")
		return false, err
	}

	rows, err := db.Query(`select count(token) from tokens where token=$1`, oneTimeToken)
	if err != nil {
		return false, err
	}

	rows.Next()
	var res int
	rows.Scan(&res)
	if res != 1 {
		return false, nil
	}

	return true, nil
}

func deleteOneTimeToken(oneTimeToken string) error {
	db, err := models.WhichDB()
	defer db.Close()

	if err != nil {
		log.Println("ERROR: failed to connect DB while deleteing OneTimeToken")
		return err
	}
	res, err := db.Exec(`delete from tokens where token=$1`, oneTimeToken)
	if err != nil {
		log.Println("ERROR: failed to delete onetimetoken =>", oneTimeToken)
		return err
	}
	deleted, _ := res.RowsAffected()
	if deleted != 1 {
		return fmt.Errorf("ERROR: deleted rows is not 1 => %d", deleted)
	}
	return nil
}

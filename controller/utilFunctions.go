package controller

import (
	"crypto/rand"
	"encoding/base64"
	"log"
	"net/http"
	"regexp"
)

func generateRandomToken() (string, error) {
	// 32byteのトークンを作成する
	b := make([]byte, 32)
	_, err := rand.Read(b)
	if err != nil {
		log.Println("ERROR: failed to get secure bytes")
		return "", err
	}

	// URLEncodingは_が含まれてしまうためStdEncodingに変更しました
	// ハッシュを計算するときに_で分割するため_が含まれていると都合が悪いです
	return base64.StdEncoding.EncodeToString(b), nil
}

// getIPAddress はhttp.RemoteAddrからポート番号を取り除きIPアドレスのみを返します
func getIPAddress(r *http.Request) string {
	re := regexp.MustCompile(`:\d+$`)
	return re.Split(r.RemoteAddr, -1)[0]
}

func requestPathIsRoot(r *http.Request) bool {
	if r.RequestURI != "/" {
		return false
	}

	return true
}

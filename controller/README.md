# Controller パッケージ

現状は全部の処理をつめこんでます

## 構成

### main.go

http.HandleFunc

- Post()

### handlePosts.go

投稿を処理する関数

- createPost()
- getPosts()

### oneTimeToken.go

OneTimeTokenを処理する関数

- saveOneTimeToken()
- isValidOneTimeToken(oneTimeToken string) (bool, error)
- deleteOneTimeToken(oneTimeToken string) error

### trackingID.go

TrackingIDを処理する関数

- isValidTrackingID(r *http.Request) bool
- addTrackingID(w http.ResponseWriter, r *http.Request)

### utilFunctions.go

パッケージ全体で使用する関数

- generateRandomToken() (string, error)
- getIPAddress(r *http.Request) string
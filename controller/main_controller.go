package controller

import (
	"html/template"
	"log"
	"net/http"
	"time"

	_ "github.com/lib/pq" // 直接使用しないためブランクインポートにしています
	"gitlab.com/gorilla_morimoto/board/models"
)

const (
	base        = "./views/base.html"
	postContent = "./views/post-content.html"
	logo        = "./views/_logo.html"
	send        = "./views/_send.html"
	content     = "./views/_content.html"
)

// HandleFuncs

// Post は"/"にアクセスしたときの処理
// POSTでアクセスしたときは、フォームの"content"の値を受け取って
// リクエストを"GET: /posts/に302でリダイレクトさせる
func Post(w http.ResponseWriter, r *http.Request) {
	if !requestPathIsRoot(r) {
		http.NotFound(w, r)
		// 最初に実装したときにはこのreturnを忘れていて
		// "404 page not found" 以下にHTMLタグがテキストで表示されてしまった
		return
	}

	switch r.Method {
	case "GET":
		log.Println("DEBUG: request to /posts in GET method from", r.RemoteAddr)

		trackingID := addTrackingID(w, r)

		oneTimeToken, err := generateRandomToken()
		// errのときはリトライ
		if err != nil {
			oneTimeToken, _ = generateRandomToken()
		}
		log.Println("DEBUG: generate OneTimeToken =>", oneTimeToken)

		t := template.Must(template.ParseFiles(postContent))

		posts, err := getPosts()
		if err != nil {
			log.Println("ERROR:", err)
		}

		err = saveOneTimeToken(oneTimeToken)
		// リトライしてだめならログを上げる
		// 正規の書き込みができなくなってしまうのでログレベルはFATALにしました
		if err != nil {
			err = saveOneTimeToken(oneTimeToken)
			if err != nil {
				log.Println("FATAL: failed to save OneTimeToken =>", oneTimeToken)
			}
		}
		log.Printf("DEBUG: save OneTimeToken: %s\n", oneTimeToken)

		d := models.Data{
			Posts:             posts,
			OneTimeToken:      oneTimeToken,
			SessionTrackingID: trackingID[0:8],
		}
		log.Println("DEBUG: SessionTrackingID:", trackingID[0:8])
		t.Execute(w, d)

	case "POST":
		log.Println("DEBUG: request to / in POST method from", r.RemoteAddr)

		if !isValidTrackingID(r) {
			log.Println("ERROR: invalid tracking_id")
			w.WriteHeader(http.StatusBadRequest)
			addTrackingID(w, r)
			w.Write([]byte("invalid request"))
			return
		}

		c, err := r.Cookie("tracking_id")
		if err != nil {
			log.Println("ERROR: invalid POST request")
			w.WriteHeader(http.StatusBadRequest)
			addTrackingID(w, r)
			w.Write([]byte("invalid request"))
			return
		}

		// リクエストについてきた"one-time-token"が正規のものかを判断し、
		// 不正なものならエラー画面を表示、
		// 正規のものならDBから該当のトークンを削除する
		oneTimeToken := r.FormValue("one-time-token")
		validOrNot, err := isValidOneTimeToken(oneTimeToken)
		if err != nil {
			validOrNot, err = isValidOneTimeToken(oneTimeToken)
		}
		if validOrNot {
			err = deleteOneTimeToken(oneTimeToken)
			if err != nil {
				log.Println(err)
			}
		} else {
			log.Println("ERROR: POST request with an invalid OneTimeToken")
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("invalid request"))
			return
		}

		trackingID := c.Value

		date := time.Now()
		userName := r.FormValue("user-name")
		content := r.FormValue("content")

		log.Println("DEBUG: user-name =>", r.FormValue("user-name"))
		log.Println("DEBUG: content =>", r.FormValue("content"))

		err = createPost(userName, content, trackingID, date)
		if err != nil {
			log.Println("FATAL:", err)
			w.Write([]byte("failed to post"))
		}

		err = deleteOneTimeToken(oneTimeToken)
		if err != nil {
			log.Println("DEBUG: retry to delete OneTimeToken")
			deleteOneTimeToken(oneTimeToken)
		}
		log.Println("DEBUG: delete OneTimeToken =>", oneTimeToken)

		http.Redirect(w, r, "/", http.StatusSeeOther)

	default:
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid method to /posts"))
		log.Println("DEBUG: request to /posts in neither GET nor POST method")
	}
}

func Delete(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.NotFound(w, r)
		return
	}
	var postID string

	log.Printf("DEBUG: request to %s in POST method from %s\n", r.RequestURI, r.RemoteAddr)

	oneTimeToken := r.FormValue("one-time-token")
	validOrNot, err := isValidOneTimeToken(oneTimeToken)
	if err != nil {
		log.Println("ERROR: failed to distinguish OnetimeToken is valid or not")
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid request"))
	}
	if validOrNot {
		postID = r.FormValue("delete-post-id")
		if postID == "" {
			log.Println("ERROR: delete-post-id is empty")
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("invalid request"))
		}
		err = deletePost(postID)
		if err != nil {
			log.Println(err)
		}
	} else {
		log.Println("ERROR: DELETE request with an invalid OneTimeToken")
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid request"))
		return
	}

	err = deleteOneTimeToken(oneTimeToken)
	if err != nil {
		log.Println("DEBUG: retry to delete OneTimeToken")
		deleteOneTimeToken(oneTimeToken)
	}
	log.Println("DEBUG: delete OneTimeToken =>", oneTimeToken)
	log.Println("DEBUG: successfully delete postID:", postID)

	http.Redirect(w, r, "/", http.StatusSeeOther)
}

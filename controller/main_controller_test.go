package controller

import (
	"net/http"
	"net/url"
	"strings"
	"sync"
	"testing"
)

func TestPost(t *testing.T) {
	wg := &sync.WaitGroup{}

	// GET to /posts
	wg.Add(1)
	go func() {
		res, err := http.Get("http://127.0.0.1:8000/posts")
		if res.StatusCode != 200 {
			t.Error("ERROR: the response code from /posts in GET method is not 200 =>", res.StatusCode)
		}
		if err != nil {
			t.Error("ERROR: ", err)
		}
		wg.Done()
	}()

	// POST to /posts without a tracking_id
	wg.Add(1)
	go func() {
		m := map[string][]string{"content": []string{"hoge"}}
		res, err := http.PostForm("http://127.0.0.1:8000/posts", url.Values(m))
		if res.StatusCode != 400 {
			t.Error("ERROR: the response code from /posts in POST method is not 400 =>", res.StatusCode)
		}
		if err != nil {
			t.Error("ERROR: ", err)
		}
		wg.Done()
	}()

	// TODO: send request in other methods

	wg.Wait()

}

func TestIsValidTrackingID(t *testing.T) {
	wg := &sync.WaitGroup{}

	wg.Add(1)
	// 正しいtrackingIDのとき
	go func() {
		r1, _ := http.NewRequest("POST", "http:localhost:8000", strings.NewReader("test"))

		r1.RemoteAddr = "192.168.0.1"

		r1.AddCookie(&http.Cookie{
			Name:  "tracking_id",
			Value: strings.ToLower("test_1E7780F8965E72DF833440A2ABE5B7C50600FA4CA074E1323D7EBAAD9BBC5142"),
		})

		if !isValidTrackingID(r1) {
			t.Errorf("ERROR: in a case of a valid tracking ID")
		}
		wg.Done()
	}()
	// 不正なtrackingIDのとき
	go func() {
		r2, _ := http.NewRequest("POST", "http:localhost:8000", strings.NewReader("test"))

		r2.RemoteAddr = "192.168.0.1"

		r2.AddCookie(&http.Cookie{
			Name:  "tracking_id",
			Value: strings.ToLower("test_1E7780F8965E72DF833440A2ABE5B7C50600FA4CA074E1323D7EBAAD9BBC5156"), // 末尾2文字が違います
		})

		if isValidTrackingID(r2) {
			t.Errorf("ERROR: in a case of an invalid tracking ID")
		}
		wg.Done()
	}()

	wg.Wait()
}

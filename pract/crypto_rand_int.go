package pract

import (
	"crypto/rand"
	"fmt"
	"math/big"
)

func cryptoRand() {
	for i := 0; i < 10; i++ {
		h, _ := rand.Int(rand.Reader, big.NewInt(900))
		fmt.Println(h)
	}
}

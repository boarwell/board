package pract

import (
	"database/sql"
	"fmt"
	"log"

	// なぜ_をつけるのかはhttp://kudohamu.hatenablog.com/entry/2014/11/29/121328を参照せよ
	_ "github.com/lib/pq"
)

// prepare statementのプレイスホルダーが$1であることの出典はhttp://go-database-sql.org/prepared.html

// DBごとに決まったものがあるらしい

func useDB() {
	// dataSourceNameに何を渡すのかは、"github.com/lib/pq"のgodocを参照せよ
	db, err := sql.Open("postgres", "user=postgres password=postgres dbname=postgres sslmode=disable")
	if err != nil {
		log.Fatalln(err)
	}
	_, err = db.Exec("drop table test;")
	if err != nil {
		log.Println("ERROR: drop table test; =>", err)
	}

	_, err = db.Exec(`create table test (id serial primary key not null, content text not null)`)
	if err != nil {
		log.Fatalln("ERROR: create table")
	}

	// ここでvalues内の文字列に""を使っていたためはまった
	// SQLでのクオートはシングルでした
	_, err = db.Exec(`insert into test(content) values('hoge is not hogera')`)
	if err != nil {
		log.Fatalln("ERROR: insert into test =>", err)
	}
	_, err = db.Exec(`insert into test(content) values('fugas are not fugathies')`)

	rows, err := db.Query(`select * from test`)
	for rows.Next() {
		// なぜ先に変数を用意するのか、
		// なぜScanにはアドレスを渡すのか、
		// 上記については、https://golang.org/pkg/database/sql/#Rows.Scan と
		// https://astaxie.gitbooks.io/build-web-application-with-golang/ja/05.4.html のサンプルコードを参照せよ
		var id int
		var content string

		rows.Scan(&id, &content)
		fmt.Println(id, content)
	}
}

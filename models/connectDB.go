package models

import (
	"database/sql"
	"errors"
	"log"
	"os"

	_ "github.com/lib/pq"
)

func WhichDB() (*sql.DB, error) {
	var (
		db  *sql.DB
		err error
	)

	env := os.Getenv("DATABASE_URL")
	if env == "" {
		db, err = connectToLocal()
	} else {
		db, err = connectToHeroku()
	}

	return db, err
}

func connectToHeroku() (*sql.DB, error) {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Println("ERROR: failed to connect DB")
		return nil, err
	}

	return db, err
}

func connectToLocal() (*sql.DB, error) {
	db, err := sql.Open("postgres", "user=postgres password=postgres dbname=postgres sslmode=disable")
	if err != nil {
		log.Println("FATAL: failed to connect to DB =>", err)
		return nil, errors.New("DB-CONNECTION")
	}

	return db, err
}

package models

import (
	"log"
	"time"
)

// ResetPosts はDBをリセットするための関数
func ResetPosts() {
	dropTable()
	log.Println("DEBUG: drop table \"test\"")
	CreateTable()
	createInitPost()
	log.Println("DEBUG: create initial post")
}

func CreateTable() error {
	db, _ := WhichDB()
	defer db.Close()

	_, err := db.Exec(`create table if not exists test(id serial primary key not null, user_names varchar(32) not null, contents text not null, tracking_id varchar(128) not null, date timestamp not null)`)

	if err != nil {
		log.Println("ERROR: create table test")
		return err
	}

	_, errTokensTable := db.Exec(`create table if not exists tokens(token varchar(64) not null primary key, created_at timestamp);`)

	if errTokensTable != nil {
		log.Println("ERROR: create table tokens")
	}

	return nil
}

func dropTable() error {
	db, err := WhichDB()
	defer db.Close()

	_, err = db.Exec(`drop table if exists test`)
	if err != nil {
		log.Println("ERROR: drop table test")
		return err
	}

	return nil
}

func createInitPost() {
	db, _ := WhichDB()
	defer db.Close()

	stmt, _ := db.Prepare(`insert into test(user_names, contents, tracking_id, date) values($1, $2, $3, $4)`)

	_, err := stmt.Exec("名無し", "自分の投稿はあとから削除できます。また、24時間ごとにDBのリセットを行っています。", "kNsjzz9L", time.Now().Format("2006-01-02 15:04:05"))
	if err != nil {
		log.Println("ERROR:", err)
	}
}

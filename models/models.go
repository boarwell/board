package models

// Struct Definitions

// Data はテンプレートに渡されるデータ
type Data struct {
	Posts []Post
	// OneTimeToken
	// create table tokens(token varchar(64) not null primary key, created_at timestamp);
	OneTimeToken      string
	SessionTrackingID string
}

// Post は投稿内容についてのデータ
// create table test(id serial primary key not null, user_names varchar(32) not null, contents text not null, tracking_id varchar(128) not null, date timestamp not null);
type Post struct {
	ID         int
	Username   string
	Content    string
	Date       string
	TrackingID string
}

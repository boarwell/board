# 実装メモ

## テンプレートの名前にhtmlタグの名前を使ってはいけない

2018-02-02

ちゃんと検証したわけではないのですが、たぶんそうっぽいです。

```go

template.ParseFiles("textarea.html")

```

というテンプレート名を当初使っていたのですが、このテンプレートをExecuteすると、画面が真っ白になってしまいました。こいつを外してほかのテンプレートを使用すると、ちゃんと描画されるのでたぶんこれが原因だと思われます。

これも検証してはいませんが、`h1`とか、既存のHTMLタグと同じ名前だとだめなような気がします。

## 埋め込まれたテンプレートに値を渡す

[公式のドキュメント](https://golang.org/pkg/text/template/)の`Actions`のところを見れば一発でわかるのですが、一応メモしておきます。

```go

{{template "post.html" .Posts}}

```

こんな感じで、テンプレート名の後ろにフィールド名を書けば値を渡せます。

`post.html`のほうでは、

```html

{{range .}}
<h3>Posted Username</h3>
<p>post here</p>
{{end}}

```

のように、`.`で値を受け取れます。

## テンプレート内でメソッド、自分で定義した関数を使う

### メソッドを使う

テンプレートに`time.Time`型のデータを`Date`という名前で渡しているとして、`time.Time`型のメソッド、`t.Format()`を使いたい場合、以下のようにメソッド名と`()`を離して記述する。

```go

{{.Date.Format ("2006/01/02 Mon 15:04:05(MST)")}}

```

#### 参考ページ

- [template - The Go Programming Language (go 1.9.3)](https://golang.org/pkg/text/template/)の`Pipelines`のところ

### 自分で定義した関数を使う

もともと組み込みで`and`, `print`などの関数が使えるようですが、自分で定義した関数も使えるようです。

ちゃんと調べたわけではないのですが、ざっと見たところ、（たぶん）以下のようにすればいいみたいです。

- `*template.Template`の`Funcs()`メソッドに`FuncMap`を渡して呼び出す
- `FuncMap`は`map[string]interfase{}`
- キーが`string`, バリューが`function`として使う
- バリューの関数は戻り値が1つか2つ（1つは値で、もう一つは`err`）じゃないといけないみたい

#### 参考ページ

- [template - The Go Programming Language (go 1.9.3)](https://golang.org/pkg/text/template/#Template.Funcs)

- [Using Functions Inside Go Templates](https://www.calhoun.io/using-functions-inside-go-templates/)

  - このページのメイン画像がめっちゃわかりやすい
  - この画像だけでどうしたらいいのかがわかる
  - すごい
    - 2018-02-17 4:56 見直してみるとサイトのデザインが変わっていてトップの画像が消えてた……

## アクセスされたURLを知る

いくつかやりかたがあるっぽくて、それぞれ少しずつ違いがあるのでまとめます。

ここでの`r`は`http.Request`を指します

```go

log.Println("RequestURI:", r.RequestURI)
log.Println("URL.Path:", r.URL.Path)
log.Println("URL.RawPath:", r.URL.RawPath)

```

### ドキュメントでの説明

`http.Request`

```go

// RequestURI is the unmodified Request-URI of the
// Request-Line (RFC 2616, Section 5.1) as sent by the client
// to a server. Usually the URL field should be used instead.
// It is an error to set this field in an HTTP client request.
RequestURI string

```

`url.URL`

```go

Path       string    // path (relative paths may omit leading slash)
RawPath    string    // encoded path hint (see EscapedPath method)

```

`url.URL`のほうには但し書きがついていまして、以下のようになっています。

```txt

Note that the Path field is stored in decoded form: /%47%6f%2f becomes /Go/. A consequence is that it is impossible to tell which slashes in the Path were slashes in the raw URL and which were %2f. This distinction is rarely important, but when it is, code must not use Path directly. The Parse function sets both Path and RawPath in the URL it returns, and URL's String method uses RawPath if it is a valid encoding of Path, by calling the EscapedPath method.

```

ざっくりまとめると次のような感じでしょうか。

- `http.Request.RequestURI`: サーバーに送信されたままの形
- `url.URL.Path`: サーバーに送信された文字列をデコードした文字列（スラッシュが含まれるとパス区切りなのかわからなくなる
- `url.URL.RawPath`: サーバーに送信されたままの形?

実際に試してみます。

### `/hoge`にアクセスした時

出力はそれぞれ以下のようになります。

```txt

2018/02/07 16:20:56 RequestURI: /hoge
2018/02/07 16:20:56 URL.Path: /hoge
2018/02/07 16:20:56 URL.RawPath:

```

この場合は、上2つが同じで、`URL.RawPath`が表示されません。

### `/日本語`にアクセスした時

```txt

2018/02/07 16:26:51 RequestURI: /%E6%97%A5%E6%9C%AC%E8%AA%9E
2018/02/07 16:26:51 URL.Path: /日本語
2018/02/07 16:26:51 URL.RawPath:

```

`RequestURI`はエンコードされた文字列の一方で、`URL.Path`のほうはブラウザのURLに記述したとおりの出力になっています。

ブラウザ（Chrome）には「日本語」と入力したのですが、サーバーに送信する際に自動でエンコードしてくれた、という解釈でいいのでしょうか。

`URL.Path`はそれをデコードして表示してくれています。

今回も`URL.RawPath`は空でした。

### `/%E6%97%A5%E6%9C%AC%E8%AA%9E`にアクセスした時

`/日本語`をエンコードした`/%E6%97%A5%E6%9C%AC%E8%AA%9E`にアクセスします。

```txt

2018/02/07 16:36:30 RequestURI: /%E6%97%A5%E6%9C%AC%E8%AA%9E
2018/02/07 16:36:30 URL.Path: /日本語
2018/02/07 16:36:30 URL.RawPath:

```

`/日本語`にアクセスしたときと同じようです。

### `/12%2f34`(12/34)にアクセスした時

今度はエンコードされた`/`を含むURLにアクセスしてみます。

```txt

2018/02/07 16:37:50 RequestURI: /12%2f34
2018/02/07 16:37:50 URL.Path: /12/34
2018/02/07 16:37:50 URL.RawPath: /12%2f34

```

すると、初めて`URL.RawPath`に出力がありました。

## まとめ

- ルーティングに使うためにリクエストされたURLを知りたいだけなら`http.Request.RequestURI`を使えばよさそう
- URLに含まれている文字列（スラッシュが含まれうる）を使いたい場合
  - `url.URL.RawPath`が空なら`url.URL.Path`を使う、そうでなければ`url.URL.RawPath`を使う
  - ただし、`url.URL.RawPath`を使う場合はスラッシュをデコードする必要がある

という感じでいかがでしょうか。

## openSUSEでPostgreSQLを使う

### インストール

`sudo zypper install postgresql-server postgres`

`postgresql`だけだとサービスを起動できないです。
-> `sudo systemctl start postgresql.service`ができない

### 問題点

`pq: Ident authentication failed for user "postgres"`というエラーが出る

### 対処

#### 設定ファイルを書き換える

`/var/lib/pgsql/data/`にある`pg_hba.conf`を書き換える

Ubuntuとは場所が違うっぽい？[参考](https://qiita.com/tomlla/items/9fa2feab1b9bd8749584)

```conf
# "local" is for Unix domain socket connections only
local   all             all                                     md5
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
# IPv6 local connections:
host    all             all             ::1/128                 md5
```

上記で`md5`となっているところをが`ident`などとなっているはずなので、そこを修正する

あとは`psql`のサービスを再起動すること

### 参考ページ

- https://en.opensuse.org/SDB:PostgreSQL#Post_install_configuration
- https://qiita.com/tomlla/items/9fa2feab1b9bd8749584
- https://qiita.com/pugiemonn/items/7ec47bc82bd56b0458b9

## 懸念点

- なりすましはできないが、同一性の否定はできる
  - CookieをいじればべつのIDが割り振られてしまうため、同一性の担保ができない
  - IPアドレスでIDを生成すればよかったのかもしれない
  - もとになったN予備校のアプリでは匿名掲示板だけどログインが必要、となっていたためこれでよかったが、今回はログイン機能を実装しなかったためこうなった

## template内で複数のフィールドの値を使う

```go

type Data struct {
	Posts []Post
	// OneTimeToken
	// create table tokens(token varchar(64) not null primary key, created_at timestamp);
	OneTimeToken      string
	SessionTrackingID string
}

// Post は投稿内容についてのデータ
// create table test(id serial primary key not null, user_names varchar(32) not null, contents text not null, tracking_id varchar(128) not null, date timestamp not null);
type Post struct {
	ID         int
	Username   string
	Content    string
	Date       string
	TrackingID string
}

```

こんな感じの構造体があるとして、テンプレートには`Data`を渡します。

ここでやりたいこととしては、書き込みの作者（？）なら自分の書き込みを削除できる、という機能を実装することです。`.Post.TrackingID`には書き込んだ人のIDが入っており、`.SessionTrackingID`には現在ページを見ている人のIDが入っています。

2つの同値判定をして削除ボタンを表示するかどうかを決めたいところですが、すべての書き込みを表示するために`{{range .Posts}}`を使っており、その内部では`.SessionTrackingID`にアクセスできません。

わかりづらいと思いますが、以下のような感じになっています。

```html

{{range .Posts}}
<div class="post card">
    <div class="card-content">
        <p class="post-metadata">{{.ID}}:
            <span class="green-text">{{.Username}}</span> {{.Date}}
            <strong>ID:{{.TrackingID}}</strong>
        </p>
        <div class="content-body-wrapper">
            <p class="content-body">{{.Content}}</p>
        </div>
        <!-- ここで閲覧者と書き込んだ人の比較をしたい -->
        <div class="delete-button-wrapper">
            <form action="/delete" method="post">
                <input type="hidden" name="id" value="{{.ID}}">
                <button class="btn red lighten-1" type="submit">削除</button>
            </form>
        </div>
    </div>
</div>
{{end}}

```

### withを使ってみる

結論から言うとだめでした。試したのは以下のようなコードです。

```html

{{with $sessionTrackingID := .SessionTrackingID}}
    <div class="posts-container">
        {{range .Posts}}
        <div class="post card">
            <div class="card-content">
                <p class="post-metadata">{{.ID}}:
                    <span class="green-text">{{.Username}}</span> {{.Date}}
                    <strong>ID:{{.TrackingID}}</strong>
                </p>
                <div class="content-body-wrapper">
                    <p class="content-body">{{.Content}}</p>
                </div>
                {{if eq .TrackingID $sessionTrackingID}}
                <div class="delete-button-wrapper">
                    <form action="/delete" method="post">
                        <input type="hidden" name="id" value="{{.ID}}">
                        <button class="btn red lighten-1" type="submit">削除</button>
                    </form>
                </div>
                {{end}}
            </div>
        </div>
        {{end}}
    </div>
<br>
{{end}}

```

たぶん冷静に考えたらわかると思うのですが、`with`の中で`{{range .Posts}}`をすると`.`には`.SessionTrackingID`の値が入っているため、`range .SessionTrackingID.Posts`と解釈されてしまい正しく表示されません。

### 単純に変数を宣言できる

[ドキュメント](https://golang.org/pkg/text/template/)ではVariablesについての以下のように記載されています。

> A pipeline inside an action may initialize a variable to capture the result.

変数はaction（`if`とか`range`, `with`）の中で宣言できるようなのですが、actionの一覧を見るとどれも文字通り何らかのアクションを実行してしまうので、単純に変数を宣言することが難しそうでした。

唯一それっぽかった`{{pipeline}}`というactionも説明が次のようになっており、「表示はさせたくないんだよなあ」という感じでした。

> {{pipeline}}
>	The default textual representation (the same as would be
>	printed by fmt.Print) of the value of the pipeline is copied
>	to the output.

が、ものは試しで`{{$sessionTrackingID := .SessionTrackingID}}`を試してみたところ普通に変数を宣言でき別のactionの中で参照できました。

```html

<!-- 単に宣言して代入するだけでもいいらしい -->
{{$sessionTrackingID := .SessionTrackingID}}
<div class="posts-container">
    {{range .Posts}}
    <div class="post card">
        <div class="card-content">
            <p class="post-metadata">{{.ID}}:
                <span class="green-text">{{.Username}}</span> {{.Date}}
                <strong>ID:{{.TrackingID}}</strong>
            </p>
            <div class="content-body-wrapper">
                <p class="content-body">{{.Content}}</p>
            </div>
            <!-- range .Postsの中で使える -->
            {{if eq .TrackingID $sessionTrackingID}}
            <div class="delete-button-wrapper">
                <form action="/delete" method="post">
                    <input type="hidden" name="id" value="{{.ID}}">
                    <button class="btn red lighten-1" type="submit">削除</button>
                </form>
            </div>
            {{end}}
        </div>
    </div>
    {{end}}
</div>

```

### ドキュメントに書いてあった

変数のスコープはどうなっているのかを調べてみたところ、以下のような記述を見つけてしまいました。

> A variable's scope extends to the "end" action of the control structure ("if", "with", or "range") in which it is declared, **or to the end of the template if there is no such control structure.** A template invocation does not inherit variables from the point of its invocation.

というわけで、テンプレートの中で複数のフィールドの値を使いたいときは単純に変数に代入してしまえばよさそうです。

### sql.Open()の戻り値のエラーでは接続失敗を検知できない？

2018-03-28

Herokuにデプロイするときは、DBの接続に`sql.Open("postgres", os.Getenv("DATABASE_URL"))`を使いますが、ここを`DATABESE_URL`とタイポしていてずっと接続がうまく行きませんでした。

当然`DATABESE_URL`なんていう環境変数は設定されていないでしょうから空文字が帰ってくるわけです。そこでDB接続はエラーになってくれなくては困るので、エラー検知のために以下のようにコードを書いていました。


```go

func WhichDB() (*sql.DB, error) {
	var (
		db  *sql.DB
		err error
	)

	env := os.Getenv("DATABASE_URL")
	if env == "" {
		db, err = connectToLocal()
	} else {
		db, err = connectToHeroku()
	}

	return db, err
}

func connectToHeroku() (*sql.DB, error) {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Println("ERROR: failed to connect DB")
		return nil, err
	}

	return db, err
}

func connectToLocal() (*sql.DB, error) {
	db, err := sql.Open("postgres", "user=postgres password=postgres dbname=postgres sslmode=disable")
	if err != nil {
		log.Println("FATAL: failed to connect to DB =>", err)
		return nil, errors.New("DB-CONNECTION")
	}

	return db, err
}

```

DB接続したい関数は`WhichDB()`を呼びます。環境変数が設定されていればHeroku環境なので`connectToHeroku()`を呼び出し、そうでなければローカルに接続しようとします。

このコードの`connectToHeroku()`関数のなかの`os.Getenv()`に誤字があったので失敗していたのですが、そのあとのエラーチェックで引っかかっていませんでした。

具体的に言うと、ログでは`ERROR: failed to connect DB`という文字列が出ていませんでした。

よくわかりませんが、Goのsqlパッケージはこのあたりの面倒は見てくれないのですかね。そのあたりはドライバのパッケージでチェックするべき、みたいな感じ？

たしかなことはちゃんと検証してみないとわかりませんが、そこのエラーに気がつけずに1日格闘していました。
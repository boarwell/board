package main

import (
	"time"

	"gitlab.com/gorilla_morimoto/board/models"
	"gitlab.com/gorilla_morimoto/board/routes"
)

func main() {
	go routes.RunServer()
	models.CreateTable()

	for {
		models.ResetPosts()
		time.Sleep(24 * time.Hour)
	}
}
